// Завдання 1
const words = ["travel", "hello", "eat", "ski", "lift"];
const count = words.filter((word) => word.length > 3).length;
console.log(count);

// Завдання 2
const people = [
  { name: "Ivan", age: 25, sex: "man" },
  { name: "Olya", age: 31, sex: "woman" },
  { name: "Alex", age: 27, sex: "man" },
  { name: "Vika", age: 33, sex: "woman" },
];

const males = people.filter((person) => person.sex === "man");
console.log(males);

// Завдання 3
function filterBy(arr, type) {
  return arr.filter((item) => typeof item !== type);
}

const mixedArray = ["hello", "world", 23, "23", null];
const filteredArray = filterBy(mixedArray, "string");
console.log(filteredArray);
